@extends('master')
@section('title')
    <h1>Halaman List Film</h1>
@endsection
@section('content')
@auth
<a href="/film/create" class="btn btn-primary mb-3">Tambah Film</a>
@endauth

    <div class="row">
        @forelse ($film as $item)
        <div class="col-2 d-flex align-items-stretch">
            <div class="card" style="width: 15rem;">
                <img class="card-img-top" src="{{asset('gambar/'. $item->poster)}}" alt="{{$item->poster}}">
                <div class="card-body">
                <span class="badge bg-info">{{$item->genre->nama}}</span>
                  <h3>{{$item->judul}}</h3>
                  <p class="card-text">{{Str::limit($item->ringkasan, 50, $end=" ....")}}</p>
                 
                  <form action="film/{{$item->id}}" method="POST">
                    <a href="/film/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                    @auth
                    <a href="/film/{{$item->id}}/edit" class="btn btn-info btn-sm">Edit</a>
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    @endauth
                </form>
                </div>
              </div>
        </div>
        @empty
            <h4>Data Film Belum Ada</h4>
        @endforelse
    </div>

@endsection
