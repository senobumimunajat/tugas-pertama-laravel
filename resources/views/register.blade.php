@extends('master')
@section('title')
    <h1>Buat Account Baru</h1>
@endsection
@section('content')
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label for="fname">First Name:</label><br>
        <input type="text" id="fname" name="fname"><br><br>
        <label for="lname">Last Name:</label><br>
        <input type="text" id="lname" name="lname"><br><br>
        <label for="gender">Gender</label><br>
        <input type="radio" id="gender" name="gender" value="male">
        <label for="gender">Male</label><br>
        <input type="radio" id="gender" name="gender" value="female">
        <label for="gender">Female</label><br>
        <input type="radio" id="gender" name="gender" value="other">
        <label for="gender">Other</label><br><br>
        <label for="nationality">Nationality : </label><br><br>
        <select id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="other">Other</option>
        </select><br><br>
        <label for="speaking">Language Speaking : </label><br><br>
        <input type="checkbox" id="lang1" name="lang1" value="indonesia">
        <label for="lang1"> Bahasa Indonesia</label><br>
        <input type="checkbox" id="lang2" name="lang2" value="english">
        <label for="lang2"> English </label><br>
        <input type="checkbox" id="otherlang" name="otherlang" value="other">
        <label for="otherlang"> Other </label><br><br>
        <label>Bio</label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection