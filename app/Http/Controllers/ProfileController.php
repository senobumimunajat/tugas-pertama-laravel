<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Profile;

class ProfileController extends Controller
{
    public function index(){
        $profile = Profile::where('user_id', Auth::id())->first();

        return view('profile.index', compact('profile'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'umur' => 'required|integer|max:100',
            'bio' => 'required',
            'alamat' => 'required',
        ],
        [
            'umur.required'  => 'Mohon Isi Umur Profile',
            'umur.max' => 'Umur Tidak Boleh Lebih Dari 100',
            'bio.required' => 'Mohon isi Biodata Profile',
            'alamat.required' => 'Mohon isi Alamat Profile',
        ]
    );
    $profile = Profile::find($id);
    $profile->umur = $request['umur'];
    $profile->bio = $request['bio'];
    $profile->alamat = $request['alamat'];
    $profile->save();

    return redirect('/profil');
    }
}
