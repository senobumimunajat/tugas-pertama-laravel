@extends('master')
@section('title')
    <h1>Halaman Update Profile</h1>
@endsection
@section('content')

<form method="POST" action="/profil/{{$profile->id}}">
    @csrf
    @method('PUT')
    <div class="form-group mb-3">   
        <label class="form-label">Nama User</label>
        <input type="text"  value="{{$profile->user->username}}"  class="form-control" disabled>
      </div>
      <div class="form-group mb-3">   
        <label class="form-label">Email User</label>
        <input type="text"  value="{{$profile->user->email}}" class="form-control" disabled>
      </div>
      <div class="form-group mb-3">   
        <label class="form-label">Umur Profile</label>
        <input type="number" name="umur" value="{{$profile->umur}}" min="1" max="100" class="form-control" placeholder="Umur 1 s/d 100">
      </div>
      @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group mb-3">
        <label class="form-label">Biodata</label>
        <textarea name="bio" cols="30" rows="10" class="form-control" placeholder="Isi Biodata Profile">{{$profile->bio}}</textarea>
      </div>
      @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group mb-3">
        <label class="form-label">Alamat</label>
        <textarea name="alamat" cols="30" rows="10" class="form-control" placeholder="Isi Alamat">{{$profile->alamat}}</textarea>
      </div>
      @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection
