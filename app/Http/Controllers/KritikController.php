<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Kritik;

class KritikController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'content' => 'required',
            'point' => 'required|integer|digits_between:1,2|min:0|max:10',
        ],
        [
            'content.required' => 'Mohon Isi Komentar Film',
            'point.required' => 'Mohon Isi Point Film',
            'point.digits_between' => 'Point Maximal 2 Digits',
            'point.min' => 'Point Manimal 0',
            'point.max' => 'Point Maximal 10',
        ]);
        $kritik = new Kritik;
        $kritik->user_id = Auth::id();
        $kritik->film_id = $request->film_id;
        $kritik->content = $request->content;
        $kritik->point = $request->point;

        $kritik->save();

        return redirect()->back();
    }
}
