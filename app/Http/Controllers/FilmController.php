<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Film;
use File;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::all();
        return view('film.index', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = DB::table('genre')->get();
        return view('film.create', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|max:45|',
            'ringkasan' => 'required',
            'tahun' => 'required|integer|digits:4|min:1900|max:'.(date('Y')),
            'poster' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'genre_id' => 'required',
        ],
        [
            'judul.required' => 'Mohon Isi Nama Film',
            'judul.max' => 'Karakter Tidak Boleh Lebih Dari 45',
            'ringkasan.required' => 'Mohon Isi Ringkasan Film',
            'tahun.required' => 'Mohon Isi Tahun Film',
            'tahun.digits' => 'Periksa Kembali Penulisan Tahun',
            'poster.required' => 'Mohon Isi Gambar Film',
            'genre_id.required' => 'Mohon Isi Genre Film',

        ]);

        $posterName = time().'.'.$request->poster->extension();

        $request->poster->move(public_path('gambar'), $posterName);

        $film = new Film;
 
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->poster = $posterName;
        $film->genre_id = $request->genre_id;
 
        $film->save();

        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $genre = DB::table('genre')->get();
        $film = Film::find($id);
        return view('film.show', compact('film','genre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = DB::table('genre')->get();
        $film = Film::find($id);
        return view('film.edit', compact('film','genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|max:45|',
            'ringkasan' => 'required',
            'tahun' => 'required|integer|digits:4|min:1900|max:'.(date('Y')),
            'poster' => 'image|mimes:jpeg,png,jpg|max:2048',
            'genre_id' => 'required',
        ],
        [
            'judul.required' => 'Mohon Isi Nama Film',
            'judul.max' => 'Karakter Tidak Boleh Lebih Dari 45',
            'ringkasan.required' => 'Mohon Isi Ringkasan Film',
            'tahun.required' => 'Mohon Isi Tahun Film',
            'tahun.digits' => 'Periksa Kembali Penulisan Tahun',
            'genre_id.required' => 'Mohon Isi Genre Film',

        ]);
        $film = Film::find($id);
        if($request->has('poster')){
            $posterName = time().'.'.$request->poster->extension();
            $request->poster->move(public_path('gambar'), $posterName);
            $film->judul = $request->judul;
            $film->ringkasan = $request->ringkasan;
            $film->tahun = $request->tahun;
            $film->poster = $posterName;
            $film->genre_id = $request->genre_id;
        } else{
            $film->judul = $request->judul;
            $film->ringkasan = $request->ringkasan;
            $film->tahun = $request->tahun;
            $film->genre_id = $request->genre_id;
        }
        $film->save();
        return redirect('/film');
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function destroy($id)
    {
        $film = Film::find($id);
        $path = "gambar/";
        File::delete($path . $film->poster);
        $film->delete();

        return redirect('/film');
    }
}
