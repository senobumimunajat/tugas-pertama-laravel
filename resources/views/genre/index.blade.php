@extends('master')
@section('title')
    <h1>Halaman List Genre</h1>
@endsection
@section('content')
@auth
    <a href="/genre/create" class="btn btn-primary mb-3">Tambah Genre</a>
@endauth


<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama Genre</th>
        <th scope="col">List Film</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($genre as $key => $item)
          <tr>
              <td>{{$key + 1}}</td>
              <td>{{$item->nama}}</td>
              <td>
                <ul>
                  @foreach ($item->film as $value)
                      <li>{{$value->judul}}</li>
                  @endforeach
                </ul>
              </td>
              <td>
                  @auth
                  <form action="/genre/{{$item->id}}" method="POST">
                    <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/genre/{{$item->id}}/edit" class="btn btn-primary btn-sm">Update</a>
                        @csrf
                        @method('delete')
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                    @endauth
              </td>
          </tr>
      @empty
          <h1>Data Tidak Ditemukan</h1>
      @endforelse
    </tbody>
  </table>

@endsection
