@extends('master')
@section('title')
    <h1>Halaman Detail Film {{$film->judul}}</h1>
@endsection
@section('content')

<img src="{{asset('gambar/'. $film->poster)}}" alt="{{$film->poster}}" width="287" height="481">
<h1>{{$film->judul}}</h1>
<h3>{{$film->tahun}}</h3>
<p>{{$film->ringkasan}}</p>

<h1>Komentar</h1>

@forelse ($film->kritik as $item)
        <div class="card">
            <div class="card-body">
                <small><strong>{{$item->user->username}}</strong></small>
                <p class="card-text">{{$item->content}}</p>
            </div>
        </div>
        @empty
        <h4>Belum Ada Komentar</h4>
    @endforelse

    

<form method="POST" action="/kritik" enctype="multipart/form-data" class="my-3">
    @csrf
      <div class="form-group mb-3">
        <label class="form-label">Content</label><br>
        <input type="hidden" name="film_id" value="{{$film->id}}">
        <textarea name="content" cols="30" rows="10" class="form-control" placeholder="Komentar Film"></textarea>
      </div>
      @error('content')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group mb-3">
        <label class="form-label">Point</label>
        <input type="number" name="point" class="form-control" placeholder="1 s/d 10">
      </div>
      @error('point')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <button type="submit" class="btn btn-primary">Submit</button>
      <a href="/film" class="btn btn-secondary">Kembali</a>
    </form>



@endsection