@extends('master')
@section('title')
    <h1>Tambah Film</h1>
@endsection
@section('content')

<form method="POST" action="/film" enctype="multipart/form-data">
@csrf
  <div class="form-group mb-3">   
    <label class="form-label">Judul Film</label>
    <input type="text" name="judul" class="form-control" placeholder="Judul Film">
  </div>
  @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group mb-3">
    <label class="form-label">Ringkasan Film</label><br>
    <textarea name="ringkasan" cols="30" rows="10" class="form-control" placeholder="Ringkasan Film"></textarea>
  </div>
  @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group mb-3">
    <label class="form-label">Tahun Film</label>
    <input type="number" name="tahun" class="form-control" placeholder="Tahun Film">
  </div>
  @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group mb-3">
    <label class="form-label">Poster Film</label>
    <input type="file" name="poster" class="form-control" placeholder="Poster Film">
  </div>
  @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group mb-3">
    <label class="form-label">Genre Film</label>
    <select name="genre_id" class="form-control" id="">
        <option value="">---Pilih Genre Film---</option>
        @foreach ($genre as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>
        @endforeach
    </select>
  </div>
  @error('genre_id')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection