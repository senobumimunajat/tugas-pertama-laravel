@extends('master')
@section('title')
    <h1>Halaman List Cast</h1>
@endsection
@section('content')
@auth
<a href="/cast/create" class="btn btn-primary mb-3">Tambah Cast</a>
@endauth
<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $item)
          <tr>
              <td>{{$key + 1}}</td>
              <td>{{$item->nama}}</td>
              <td>{{$item->umur}}</td>
              <td>{{$item->bio}}</td>
              <td>
                @auth
                  <form action="/cast/{{$item->id}}" method="POST">
                    
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-primary btn-sm">Update</a>
                        @csrf
                        @method('delete')
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        @endauth
                        <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm ">Detail</a>
                    </form>
              </td>
          </tr>
      @empty
          <h1>Data Tidak Ditemukan</h1>
      @endforelse
    </tbody>
  </table>

@endsection
