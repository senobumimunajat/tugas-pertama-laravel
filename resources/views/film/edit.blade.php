@extends('master')
@section('title')
    <h1>Edit Film</h1>
@endsection
@section('content')

<form method="POST" action="/film/{{$film->id}}" enctype="multipart/form-data">
@csrf
@method('PUT')
  <div class="form-group mb-3">   
    <label class="form-label">Judul Film</label>
    <input type="text" name="judul" value="{{$film->judul}}" class="form-control" placeholder="Judul Film">
  </div>
  @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group mb-3">
    <label class="form-label">Ringkasan Film</label><br>
    <textarea name="ringkasan" cols="30" rows="10" class="form-control">{{$film->ringkasan}}</textarea>
  </div>
  @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group mb-3">
    <label class="form-label">Tahun Film</label>
    <input type="number" name="tahun" value="{{$film->tahun}}" class="form-control" placeholder="Tahun Film">
  </div>
  @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group mb-3">
    <img src="{{asset('gambar/'. $film->poster)}}" alt="{{$film->poster}}" width="80" height="150"><br><br>
    <label class="form-label">Edit Poster Film</label>
    <input type="file" name="poster" class="form-control" placeholder="Poster Film">
  </div>
  @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group mb-3">
    <label class="form-label">Genre Film</label>
    <select name="genre_id" class="form-control" id="">
        <option value="">---Pilih Genre Film---</option>
        @foreach ($genre as $item)
            @if ($item->id === $film->genre_id)
                <option value="{{$item->id}}" selected>{{$item->nama}}</option>
            @else
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endif

        @endforeach
    </select>
  </div>
  @error('genre_id')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
  <a href="/film" class="btn btn-secondary ">Kembali</a>
</form>
@endsection