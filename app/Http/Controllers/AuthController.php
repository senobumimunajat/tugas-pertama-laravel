<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('register');
    }

    public function submit(Request $request){
        $fname = $request['fname'];
        $lname = $request['lname'];
        return view('welcome', compact('fname', 'lname'));
    }
}
