@extends('master')
@section('title')
    <h1>Update Cast</h1>
@endsection
@section('content')

<form method="POST" action="/cast/{{$cast->id}}">
@csrf
@method('put')
  <div class="form-group mb-3">   
    <label class="form-label">Nama Cast</label>
    <input type="text" name="nama" value="{{$cast->nama}}" class="form-control" placeholder="Nama Cast">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group mb-3">
    <label class="form-label">Umur</label><br>
    <input type="number" name="umur" value="{{$cast->umur}}" min="1" max="100" class="form-control" placeholder="Umur 1 s/d 100">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group mb-3">
    <label class="form-label">Biodata</label>
    <textarea name="bio" cols="30" rows="10" class="form-control" placeholder="Isi Biodata Cast">{{$cast->bio}}</textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection