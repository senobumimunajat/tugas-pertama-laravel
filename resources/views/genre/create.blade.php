@extends('master')
@section('title')
    <h1>Tambah Genre</h1>
@endsection
@section('content')

<form method="POST" action="/genre">
@csrf
  <div class="form-group mb-3">   
    <label class="form-label">Nama Genre</label>
    <input type="text" name="nama" class="form-control" placeholder="Nama Genre">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection