<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/register', 'AuthController@form');

Route::post('/welcome', 'AuthController@submit');

Route::get('/data-table', 'IndexController@DataTable');


    // CRUD Cast
    // Create
    Route::get('/cast/create', 'CastController@create'); //mengarah ke form tambah cast
    Route::post('/cast', 'CastController@store'); //menyimpan data form ke database table cast
    // Read
    Route::get('/cast', 'CastController@index'); //ambil data database dan ditampilkan
    Route::get('/cast/{cast_id}', 'CastController@show'); //Route Detail Cast
    // Update
    Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //Route ke Form Edit
    Route::put('/cast/{cast_id}', 'CastController@update'); //edit cast
    // Delete
    Route::delete('/cast/{cast_id}', 'CastController@destroy'); //Hapus Cast by ID

    // CRUD Genre
    // Create
    Route::get('/genre/create', 'GenreController@create'); 
    Route::post('/genre', 'GenreController@store');
    // Read
    Route::get('/genre', 'GenreController@index'); 
    Route::get('/genre/{genre_id}', 'GenreController@show'); 
    // Update
    Route::get('/genre/{genre_id}/edit', 'GenreController@edit'); 
    Route::put('/genre/{genre_id}', 'GenreController@update');
    // Delete
    Route::delete('/genre/{genre_id}', 'GenreController@destroy');

    // Update Profile
    Route::resource('profil','ProfileController')->only([
        'index', 'update'
    ]);

    Route::resource('kritik','KritikController')->only([
        'index', 'store'
    ]);


    // CRUD Film
    // Create
    Route::resource('film','FilmController');

// Auth
Auth::routes();


