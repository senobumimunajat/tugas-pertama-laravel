@extends('master')
@section('title')
    <h1>Halaman Detail Genre {{$genre->nama}}</h1>
@endsection
@section('content')

<h3>{{$genre->nama}}</h1>

<div class="row">
    @foreach ($genre->film as $item)
    <div class="col-2">
        <div class="card" style="width: 15rem;">
            <img class="card-img-top" src="{{asset('gambar/'. $item->poster)}}" alt="{{$item->poster}}">
            <div class="card-body">
              <h3>{{$item->judul}}</h3>
              <p class="card-text">{{$item->ringkasan}}</p>
            </div>
          </div>
    </div>
    @endforeach
</div>
 

@endsection